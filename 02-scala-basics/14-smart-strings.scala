val lyrics = "I see trees of green, \n" +
             "Red roses too, \n" +
             "I see them bloom, \n" +
             "For me and you, \n" +
             "And I think to myself, \n" +
             "What a wonderful world"
println(lyrics)
println()

val lyrics1 = """I see trees of green,
             Red roses too,
             I see them bloom,
             For me and you,
             And I think to myself,
             What a wonderful world"""
println(lyrics1)
println()

val lyrics2 = """I see trees of green,
             |Red roses too,
             |I see them bloom,
             |For me and you,
             |And I think to myself,
             |What a wonderful world""".stripMargin
println(lyrics2)
println()

val lyrics3 = """I see trees of green,
             @Red roses too,
             @I see them bloom,
             @For me and you,
             @And I think to myself,
             @What a wonderful world""".stripMargin('@')
println(lyrics3)
println()

val message = "We are meeting on Jun 13th of this year, and having lunch at 12:30PM"
val regexJavaStyle = "(\\s[0-9])?[0-9]:[0-5][0-9]\\s*(AM|PM)".r
val regex = """(\s|[0-9])?[0-9]:[0-5][0-9]\s*(AM|PM)""".r

println(regex.findAllIn(message).toList)
