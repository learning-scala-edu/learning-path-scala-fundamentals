val a = 100

// following fails, because cannot reassign to val
// a = 300

// var can be reassigned
var b = 400

b = 500

// but type cannot be changed
// following fails
//b = "Hello"

val ab = 400
// compiler will infer the type to be Int

val ba: Double = 400

// coercion
val aba = 400:Double

val c = 100
val d = "Hello"
val e = c + ", " + d


// Lazy val
lazy val f = {println("evaluated"); 5}
f
f
