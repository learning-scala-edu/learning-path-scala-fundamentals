val a = 99
println(s"${a} luftballons floating in the summer sky")
println(s"$a luftballons floating in the summer sky")
println()

println(s"$a + 3 luftballons floating in the summer sky")
println(s"${a + 3} luftballons floating in the summer sky")
println()

val ticketCost = 50
val bandName = "Psychedelic Furs"
println(f"The $bandName%s tickets are probably $$$ticketCost%1.2f")
println()

val percentIncrease = 20
val musicGenre = "New Wave"
println(f"The $bandName%s tickets are probably $$$ticketCost%1.2f%nThat's a $percentIncrease%% bump because everyone likes $musicGenre")
println()

println(
  f"""The $bandName%s tickets are probably $$$ticketCost%1.2f
     |That's a $percentIncrease%% bump because everyone likes $musicGenre""".stripMargin)
println()
