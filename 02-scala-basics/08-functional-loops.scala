val result1 = (1 to 100).reverse.mkString(",")
println(result1)

val result2 = (100 to 1 by -1).reverse.mkString(",")
println(result2)

println((100 to 1 by -1).reverse.mkString(","))
