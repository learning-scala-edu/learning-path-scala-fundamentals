class Employee(firstName: String, lastName: String, department: Department)

class Department(name: String)

// to compile execute following command:
// scalac -d classes src/*.scala

// to examine the classes execute following:
// cd classes
// javap -p Empployee